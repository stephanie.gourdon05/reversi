"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var C;
(function (C) {
    C[C["Empty"] = 0] = "Empty";
    C[C["Player1"] = 1] = "Player1";
    C[C["Player2"] = 2] = "Player2";
})(C = exports.C || (exports.C = {}));
function CtoString(c) {
    switch (c) {
        case C.Empty: return 'Empty';
        case C.Player1: return 'Player1';
        case C.Player2: return 'Player2';
    }
}
exports.CtoString = CtoString;
