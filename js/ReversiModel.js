"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
// import { map } from 'rxjs/operators';
const ReversiDefinitions_1 = require("./ReversiDefinitions");
class ReversiModel {
    constructor() {
        this.boardSubj = new rxjs_1.BehaviorSubject(this);
        this.boardObs = this.boardSubj.asObservable();
        this.initBoard();
    }
    getObservable() {
        return this.boardObs;
    }
    getBoard() {
        return this.board;
    }
    initBoard() {
        this.currentTurn = ReversiDefinitions_1.C.Player1;
        this.board = new Array(8).fill(0).map(l => new Array(8).fill(ReversiDefinitions_1.C.Empty));
        this.board[3][3] = this.board[4][4] = ReversiDefinitions_1.C.Player1;
        this.board[4][3] = this.board[3][4] = ReversiDefinitions_1.C.Player2;
    }
    PionsTakenIfPlayAt(x, y) {
        if (this.board[x][y] === ReversiDefinitions_1.C.Empty) {
            const otherTile = this.turn() === ReversiDefinitions_1.C.Player1 ? ReversiDefinitions_1.C.Player2 : ReversiDefinitions_1.C.Player1;
            let tilesToFlip = [];
            // à compléter
            // Parcourrir les 8 directions à partir de la case i, j
            // S'arréter dès qu'on est au bord du plateau ou que la case n'est pas otherTile
            // Accumuler dans tilesToFlip les cases qui seraient transformées si on jouait en i, j
            const Accu = (dx, dy) => {
                let px = x;
                let py = y;
                let c;
                const L = [];
                do {
                    px += dx;
                    py += dy;
                    c = this.board[px] !== undefined ? this.board[px][py] : ReversiDefinitions_1.C.Empty; //Attention, si board[px] est undefined, alors erreur
                    L.push({ x: px, y: py });
                } while (c === otherTile);
                L.pop();
                return c === this.turn() ? L : [];
            };
            const directions = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]];
            tilesToFlip = directions.map(([dx, dy]) => Accu(dx, dy))
                .reduce((L, SL) => [...L, ...SL], []);
            return tilesToFlip;
        }
        return [];
    }
    turn() {
        return this.currentTurn;
    }
    play(i, j) {
        const L = this.PionsTakenIfPlayAt(i, j);
        if (L.length) {
            // à compléter
            this.board[i][j] = this.turn();
            L.forEach(({ x, y }) => this.board[x][y] = this.turn());
            //Il faut changer le joueur courant
            //Seulement si il peut jouer
            this.currentTurn = this.turn() === ReversiDefinitions_1.C.Player1 ? ReversiDefinitions_1.C.Player2 : ReversiDefinitions_1.C.Player1;
            if (this.skipTurn()) {
                this.currentTurn = this.turn() === ReversiDefinitions_1.C.Player1 ? ReversiDefinitions_1.C.Player2 : ReversiDefinitions_1.C.Player1;
            }
            // On prévient les observeurs
            this.boardSubj.next(this);
        }
    }
    skipTurn() {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                if (this.PionsTakenIfPlayAt(i, j).length > 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
exports.ReversiModel = ReversiModel;
