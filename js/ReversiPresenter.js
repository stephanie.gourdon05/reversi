"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ReversiDefinitions_1 = require("./ReversiDefinitions");
const reversiView = `
	<table class="reversi">
    <tbody></tbody>
  </table>
`;
class ReversiPresenter {
    constructor(root, model) {
        this.root = root;
        this.model = model;
        this.tds = [];
        this.initBoard();
        // Abonnement aux évènements du modèle :
        model.getObservable().subscribe(() => this.updatePresentation());
    }
    updatePresentation() {
        // à compléter
        // Les cases (balises td) contenant un pion du joueur 1 ont la classe CSS Player1 (voir le CSS)
        // Les cases (balises td) contenant un pion du joueur 2 ont la classe CSS Player2 (voir le CSS)
        // Les cases sur lesquelles le joueur courant peut poser un pion ont la classe CSS canPlayHere
        this.model.getBoard().forEach((L, i) => {
            L.forEach((c, j) => {
                const td = this.tds[i][j];
                td.setAttribute("class", ReversiDefinitions_1.CtoString(c));
                if (this.model.PionsTakenIfPlayAt(i, j).length > 0) {
                    td.classList.add("canPlayHere");
                }
            });
        });
    }
    initBoard() {
        this.root.innerHTML = reversiView;
        // à compléter
        // Remplir le tableau aavec 8x8 cases contenant chacune une balise div
        const tbody = this.root.querySelector("tbody");
        this.model.getBoard().forEach((L, i) => {
            const tr = document.createElement("tr");
            const Ltds = [];
            L.forEach((c, j) => {
                const td = document.createElement("td");
                td.innerHTML = "<div></div>";
                tr.appendChild(td);
                Ltds.push(td);
                td.onclick = () => this.model.play(i, j);
            });
            tbody.appendChild(tr);
            this.tds.push(Ltds);
        });
        // Utiliser la fonction document.createElement
        // Stockez les balises tds dans l'attribut tds de l'objet ReversiPresenter
        this.updatePresentation();
    }
}
exports.ReversiPresenter = ReversiPresenter;
