"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ReversiModel_1 = require("./ReversiModel");
const ReversiPresenter_1 = require("./ReversiPresenter");
const ReversiDefinitions_1 = require("./ReversiDefinitions");
// ____________________________ Init ____________________________
const m = new ReversiModel_1.ReversiModel();
const p1 = new ReversiPresenter_1.ReversiPresenter(document.querySelector("div.R1"), m);
const p2 = new ReversiPresenter_1.ReversiPresenter(document.querySelector("div.R2"), m);
// ____________________________ Debugger ________________________
function cToString(c) {
    switch (c) {
        case ReversiDefinitions_1.C.Empty: return ".";
        case ReversiDefinitions_1.C.Player1: return "X";
        case ReversiDefinitions_1.C.Player2: return "O";
    }
}
function LtoString(L) {
    return L.reduce((acc, c) => `${acc}${cToString(c)}`, '');
}
function BoardtoString(b) {
    return b.map(LtoString).join("\n");
}
m.getObservable().subscribe(() => {
    const b = m.getBoard();
    console.log("_______________________");
    console.log(BoardtoString(b));
    console.log(`Player ${m.turn() === ReversiDefinitions_1.C.Player1 ? 1 : 2} (${cToString(m.turn())}) can play at:`);
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if (m.PionsTakenIfPlayAt(i, j).length > 0) {
                console.log("\t", i, ",", j);
            }
        }
    }
    console.log("_______________________");
});
// @ts-ignore
Window['debugReversi'] = m;
