import { BehaviorSubject, Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
import {Board, Turn, ReversiModelInterface, C, TileCoords} from "./ReversiDefinitions";

export class ReversiModel implements ReversiModelInterface {
    private board: Board;
    private boardSubj = new BehaviorSubject<ReversiModelInterface>(this);
    private boardObs = this.boardSubj.asObservable();
    currentTurn: Turn;

    constructor() {
        this.initBoard();
    }

    getObservable(): Observable<ReversiModelInterface> {
        return this.boardObs;
    }

    getBoard(): Board {
        return this.board;
    }

    initBoard() {
        this.currentTurn = C.Player1;
        this.board = new Array(8).fill(0).map( l => new Array<C>(8).fill(C.Empty) ) as Board;
        this.board[3][3] = this.board[4][4] = C.Player1;
        this.board[4][3] = this.board[3][4] = C.Player2;
    }

    PionsTakenIfPlayAt(x: number, y: number): TileCoords[] {
        if (this.board[x][y] === C.Empty) {
            const otherTile = this.turn() === C.Player1 ? C.Player2 : C.Player1 ;
            let tilesToFlip: TileCoords[] = [];
            // à compléter
            // Parcourrir les 8 directions à partir de la case i, j
            // S'arréter dès qu'on est au bord du plateau ou que la case n'est pas otherTile
            // Accumuler dans tilesToFlip les cases qui seraient transformées si on jouait en i, j
            const Accu = (dx: number,dy: number): TileCoords[] => {
                let px = x;
                let py = y;
                let c: C;
                const L: TileCoords[] = [];
                do{
                    px += dx;
                    py += dy;
                    c = this.board[px] !== undefined ? this.board[px][py] : C.Empty; //Attention, si board[px] est undefined, alors erreur
                    L.push({x:px, y:py});
                }while(c === otherTile);
                L.pop();
                return c === this.turn() ? L : [];
            }
            const directions = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]];
            tilesToFlip = directions.map(([dx,dy]) => Accu(dx,dy))
                                    .reduce((L, SL) => [...L, ...SL], []);
            return tilesToFlip;
        }
        return [];
    }

    turn(): Turn {
        return this.currentTurn;
    }

    play(i: number, j: number): void {
        const L = this.PionsTakenIfPlayAt(i, j);
        if (L.length) {
            // à compléter
            this.board[i][j] = this.turn();
            L.forEach(({x,y}) => this.board[x][y] = this.turn());
            //Il faut changer le joueur courant
            //Seulement si il peut jouer
            this.currentTurn = this.turn() === C.Player1 ? C.Player2 : C.Player1;
            if(this.skipTurn()){
                this.currentTurn = this.turn() === C.Player1 ? C.Player2 : C.Player1;
            }
            // On prévient les observeurs
            this.boardSubj.next(this);
        }
    }

    skipTurn(): boolean {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                if (this.PionsTakenIfPlayAt(i, j).length > 0) {
                    return false;
                }
            }
        }
        return true;
    }

}
