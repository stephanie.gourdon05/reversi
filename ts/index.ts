import {ReversiModel} from "./ReversiModel";
import {ReversiPresenter} from "./ReversiPresenter";
import {C, Board} from "./ReversiDefinitions";


// ____________________________ Init ____________________________
const m = new ReversiModel();
const p1 = new ReversiPresenter(document.querySelector("div.R1") as HTMLElement, m);
const p2 = new ReversiPresenter(document.querySelector("div.R2") as HTMLElement, m);

// ____________________________ Debugger ________________________
function cToString(c: C): string {
	switch(c) {
		case C.Empty: return ".";
		case C.Player1: return "X";
		case C.Player2: return "O";
	}
}
function LtoString(L: C[]): string {
	return L.reduce((acc, c) => `${acc}${cToString(c)}`, '');
}
function BoardtoString(b: Board): string {
	return b.map( LtoString).join("\n");
}

m.getObservable().subscribe( () => {
    const b = m.getBoard();
    console.log( "_______________________" );
    console.log( BoardtoString(b) );
    console.log(`Player ${m.turn() === C.Player1 ? 1 : 2} (${cToString(m.turn())}) can play at:`)
    for(let i=0; i<8; i++) {
        for(let j=0; j<8; j++) {
            if (m.PionsTakenIfPlayAt(i, j).length > 0) {
                console.log("\t", i, ",", j);
            }
        }
    }
    console.log( "_______________________" );
} );


// @ts-ignore
Window['debugReversi'] = m;
